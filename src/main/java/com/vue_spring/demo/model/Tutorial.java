package com.vue_spring.demo.model;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "tutorials")
public class Tutorial {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "value")
	private Long value;

	@Column(name = "published")
	private boolean published;

	@Column(name = "registry_date")
    private Timestamp registryDate;

    @Column(name = "update_date")
    private Timestamp updateDate;

	public Tutorial() {

	}

	public Tutorial(String title, String description, Long value, boolean published, Timestamp registryDate, Timestamp updateDate) {
		this.title = title;
		this.description = description;
		this.value = value;
		this.published = published;
		this.registryDate = registryDate;
		this.updateDate = updateDate;
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean isPublished) {
		this.published = isPublished;
	}

	public Timestamp getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Timestamp registryDate) {
		this.registryDate = registryDate;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Tutorial [id=" + id + ", title=" + title + ", desc=" + description + ", value=" + value +  ", published=" + published + "]";
	}
}